#Removes Adobe CC files Link from the Windows 10 Sidebar
$confirmation = Read-Host 'This script will modify registry values on your computer. Are you Sure You Want To Proceed? (y/n)'
    if ($confirmation -eq 'y') {
        $registryPath = "Registry::HKEY_CLASSES_ROOT\CLSID\{0E270DAA-1BE6-48F2-AC49-B09960AF0444}"
        $Name = "System.IsPinnedToNameSpaceTree"
        $value = "0"
        New-ItemProperty -Path $registryPath -Name $name -Value $value `
        -PropertyType DWORD -Force | Out-Null
        pause
    }
